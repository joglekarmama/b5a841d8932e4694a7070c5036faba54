import Phaser from 'phaser';
import Database from 'database-api';

let database = new Database();

class LeaderBoard extends Phaser.Scene {
  constructor() {
    super('leaderBoard');
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
    this.scores = [];
  }

  preload() {
    database
      .getLeaderBoard()
      .then((data) => {
        let sortedData = data.sort(
          (a, b) => parseInt(a.score) < parseInt(b.score)
        );
        this.scores = [...sortedData];
      })
      .catch((err) => {
        console.log(err);
      });
  }

  create() {
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;

    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on('resize', this.resize, this);

    const bg = this.add.image(0, 0, 'background').setOrigin(0, 0).setScale(5);

    const title = this.add.text(
      this.GAME_WIDTH / 2.7,
      this.GAME_HEIGHT / 10,
      'LeaderBoard',
      {
        fontFamily: 'arial',
        fontSize: '32px',
        fontStyle: 'bold',
        fill: '#000',
      }
    );

    let i,
      length = this.scores.length <= 10 ? this.scores.length : 10;
    for (i = 0; i < length; i++) {
      this.add.text(
        this.GAME_WIDTH / 4,
        this.GAME_HEIGHT / 7 + 40 * (i + 1),
        `${i + 1}         ${this.scores[i].display_name}        ${
          this.scores[i].score
        }`,
        {
          fontSize: '25px',
          fill: '#000',
        }
      );
    }

    let hoverImage = this.add.image(100, 100, 'startBall');
    hoverImage.setVisible(false);
    hoverImage.setScale(0.05);

    const backBtn = this.add.text(
      this.GAME_WIDTH / 2.8,
      this.GAME_HEIGHT / 1.5,
      'Back To Menu',
      {
        fontSize: '32px',
        fill: '#000',
      }
    );

    backBtn.setInteractive();

    backBtn.on('pointerover', () => {
      hoverImage.setVisible(true);
      hoverImage.x = backBtn.x - backBtn.width / 2.6;
      hoverImage.y = backBtn.y + 10;
    });
    backBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });
    backBtn.on('pointerdown', () => {
      // console.log('redirect to play');
      this.scene.start('bootGame');
    });
  }

  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;
    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;
    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }
}

export default LeaderBoard;

//
//
//
//
//
//
// this.load.bitmapFont('arcade', 'assets/arcade.png', 'assets/arcade.xml');

// this.add
//   .bitmapText(100, 110, 'arcade', 'RANK  SCORE   NAME')
//   .setTint(0xffffff);

// for (let i = 1; i < 6; i++) {
//   if (scores[i - 1]) {
//     this.add
//       .bitmapText(
//         100,
//         160 + 50 * i,
//         'arcade',
//         ` ${i}      ${scores[i - 1].highScore}    ${scores[i - 1].name}`
//       )
//       .setTint(0xffffff);
//   } else {
//     this.add
//       .bitmapText(100, 160 + 50 * i, 'arcade', ` ${i}      0    ---`)
//       .setTint(0xffffff);
//   }
// }
